# Linux Power Manager // pwrmgr

## Description
Windows Server has a handy way of auditing power management requests (e.g. shutdown and reboot).
<br/>I wanted this feature to be present on my Linux servers.
<br/>So since I got bored on Christmas Eve
<br/>I uh
<br/>Made this lmao
<br/>Then uh
<br/>Put stuff in this new repo
<br/>(☞ﾟヮﾟ)☞


## Installation
Run the installaation script (https://gitlab.com/amheus/linuxpowermanager/-/raw/main/installer.sh) under sudo using the following commands or similar.
```bash
cd ~ && \
wget https://gitlab.com/amheus/linuxpowermanager/-/raw/main/installer.sh && \
chmod a+x installer.sh && \
sudo ./installer.sh
```


## Uninstallation
just run the following basically
```bash
rm /usr/sbin/shutdown && rm /usr/sbin/reboot && \
mv /usr/sbin/shutdown_ /usr/sbin/shutdown && mv /usr/sbin/reboot_ /usr/sbin/reboot && \
rm -rf /var/lib/pwrmgr
```


## Support
If you have issues with this repo, please raise a ticket at https://support.scorpia.network
