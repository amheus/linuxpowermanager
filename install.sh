#!/bin/bash

clear

echo "1/3 -- creating directories"
mkdir -p /etc/pwrmgr
mkdir -p /var/lib/pwrmgr

echo "2/3 -- downloading resources"
cd /var/lib/pwrmgr
wget --quiet https://gitlab.com/amheus/linuxpowermanager/-/raw/main/pwrmgr.sh
mv pwrmgr.sh pwrmgr
chmod a+x pwrmgr


echo "3/3 -- managing symlinks"
mv /usr/sbin/shutdown /usr/sbin/shutdown_
mv /usr/sbin/reboot /usr/sbin/reboot_
ln /var/lib/pwrmgr/pwrmgr /usr/sbin/shutdown
ln /var/lib/pwrmgr/pwrmgr /usr/sbin/reboot

echo "complete"
