#!/bin/bash


if [ $0 == "/usr/sbin/shutdown" ]; then
    OPERATION_MODE="shutdown"
elif [ $0 == "/usr/sbin/reboot" ]; then
    OPERATION_MODE="reboot"
else
    exit -1
fi

if [ $1 == "-c" ]; then
    /usr/sbin/shutdown_ -c
    echo "abort"
    exit 0
fi


LOG_DIRECTORY="/var/log/pwrmgr"
LOG_FILENAME="$OPERATION_MODE.log"

clear

cat <<EOF
PPPPPPPPP WW      WW RRRRRRRRR MMM   MMM GGGGGGGG  RRRRRRRRR
PP     PP WW  WW  WW RR     RR MMM   MMM GG    GG  RR     RR
PP     PP WW  WW  WW RR     RR MMMM MMMM GG        RR     RR
PPPPPPPP  WW  WW  WW RRRRRRRR  MM MMM MM GG   GGGG RRRRRRRR
PP        WW  WW  WW RR   RR   MM     MM GG    GG  RR   RR
PP        WW  WW  WW RR    RR  MM     MM GG    GG  RR    RR
PP         WWW  WWW  RR     RR MM     MM  GGGGGG   RR     RR

a request for $OPERATION_MODE has been made
please provide a reason for this request

alternatively, enter \`cancel\` or nothing to abort $OPERATION_MODE

EOF
echo -n "> "
read REASON_FOR_OPERATION

clear

if [ "$REASON_FOR_OPERATION" == "" ] || [ "$REASON_FOR_OPERATION" == "cancel" ]
then
    /usr/sbin/shutdown_ -c
    echo "$OPERATION_MODE aborted"
    exit 0
fi

mkdir -p $LOG_DIRECTORY
touch "$LOG_DIRECTORY/$LOG_FILENAME"
echo "[$(date +"%Y-%m-%d %H:%M:%S")]~[$(whoami)] -- $REASON_FOR_OPERATION" >> "$LOG_DIRECTORY/$LOG_FILENAME"
cat <<EOF
$HOSTNAME will initate $OPERATION_MODE in one minute ($(date -d '1 mins' +"%Y-%m-%d %H:%M:%S"))

To abort the $OPERATION_MODE, run \`shutdown -c\`
EOF

if [ $OPERATION_MODE == "shutdown" ]
then
    /usr/sbin/shutdown_ -P +1 &> /dev/null
elif [ $OPERATION_MODE == "reboot" ]
then
    /usr/sbin/shutdown_ -r +1 &> /dev/null
fi
